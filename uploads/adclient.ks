cdrom
bootloader --location=mbr
network --device=link --activate --bootproto=static --ip=172.16.2.133 --netmask=255.255.255.0 --gateway=172.16.2.2 --hostname=adclient003.samdom.openqa.fedoraproject.org --nameserver=172.16.2.130
lang en_US.UTF-8
keyboard us
timezone --utc America/New_York
clearpart --all
autopart
%packages
@^server-product-environment
%end
rootpw anaconda
reboot
realm join --one-time-password=monkeys ad001.samdom.openqa.fedoraproject.org
