bootloader --location=mbr
network --bootproto=dhcp
url --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch
repo --name=updates --mirrorlist=https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f$releasever&arch=$basearch
lang en_US.UTF-8
keyboard us
timezone --utc America/New_York
clearpart --all
autopart
rootpw --plaintext weakpassword
user --name=test --password=weakpassword --plaintext --groups wheel
firstboot --enable
poweroff

%packages
@^workstation-product-environment
-selinux-policy-minimum
# FIXME: this is a workaround for https://bugzilla.redhat.com/show_bug.cgi?id=2158891
# can be dropped when https://bodhi.fedoraproject.org/updates/FEDORA-2023-4021d4c044
# is stable
cracklib-dicts
%end

%post
authselect enable-feature with-fingerprint
touch $INSTALL_ROOT/home/home_preserved
%end
