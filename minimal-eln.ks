bootloader --location=mbr
network --bootproto=dhcp
# FIXME: use mirrorlist when https://github.com/fedora-infra/mirrormanager2/issues/382 is done
url --url=https://dl.fedoraproject.org/pub/eln/1/BaseOS/$basearch/os/
repo --name=appstream --baseurl=https://dl.fedoraproject.org/pub/eln/1/AppStream/$basearch/os/
lang en_US.UTF-8
keyboard us
timezone --utc America/New_York
clearpart --all
autopart
rootpw weakpassword
poweroff

%packages
@core
%end
